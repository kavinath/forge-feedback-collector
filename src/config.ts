/**
 * Default values for which project and which issue type to create
 * These will be overridden by Forge environment variables (see index.tsx)
 */
export default {
  PROJECT_KEY: "FORGE",
  ISSUE_TYPE_NAME: "Task"
};
