# Forge Feedback Collector Macro

This simple Forge app adds a form macro to your Confluence page that allows users to submit feedback that is converted into a Jira ticket for a project on the same site.

![Form](screenshots/app-initial.png)

![Form](screenshots/app-success.png)

## Quick start

### Edit the configuration

Edit the `src/config.ts` to match the Jira Cloud project key and issue type for the project that you wish to create for submissions.

For example, the desired project from `yoursite.atlassian.net/jira/software/projects/<PROJECTKEY>` and issue type of "Task", "Bug", etc.

### Install and deploy the app
 
You will need Node.js and the Forge CLI to install this app. You can install the Forge CLI by running `npm install -g @forge/cli`.

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies.
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app into your Confluence site

## Project overview

- `config.ts`
Describes default values for the Jira issue creation behaviour. This is useful for testing the app while tunneling

- `index.tsx`
Contains the main logic of the app

- `issue.tsx`
Contains helper methods to format the body of the Jira Cloud REST API request
